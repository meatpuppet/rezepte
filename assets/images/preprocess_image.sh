#!/bin/bash

set -e

function process() {
  local IMAGE_PATH=$1
  FILENAME=$(basename "$IMAGE_PATH")
  BACKUP_PATH=/tmp/$(date +"%m-%d-%Y-%T")_$FILENAME
  echo "writing backup to $BACKUP_PATH"
  cp "$IMAGE_PATH" "$BACKUP_PATH"

  echo "removing metadata"
  exiftool -all= $IMAGE_PATH
  echo "scaling to 1000"

  # mogrify does the same as convert, but doesnt keep the original
  # ">" means only make smaller, not bigger
  mogrify -resize '1000>' $IMAGE_PATH $IMAGE_PATH
}

for IMAGE_PATH in "$@"
do
  if test -f $IMAGE_PATH; then
    process $IMAGE_PATH
  else
    echo "file not found: $IMAGE_PATH"
  fi
done
