#!/bin/bash

set -o xtrace
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PUBLIC_PATH=/tmp/rezepte_rendered

REMOTE_REPO=git@codeberg.org:meatpuppet/rezepte.git
REMOTE_BRANCH=pages

echo -e "\033[0;32mDeploying updates to GitHub...\033[0m"

# init repo if not existent, switch to branch
if [ ! -d ${PUBLIC_PATH} ]; then git clone ${REMOTE_REPO} ${PUBLIC_PATH}; fi
cd $PUBLIC_PATH && git pull && git checkout $REMOTE_BRANCH && cd -

# Build the project.
hugo --destination ${PUBLIC_PATH} # if using a theme, replace with `hugo -t <YOURTHEME>`

# Go To Public folder
cd ${PUBLIC_PATH}

# Add changes to git.
git add .

# Commit changes.
msg="rebuilding site `date`"
if [ $# -eq 1 ]
  then msg="$1"
fi


git commit -m "$msg"

# Push source and build repos.
git push origin $REMOTE_BRANCH

# Come Back up to the Project Root
cd ${DIR}
