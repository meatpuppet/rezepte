# Linsensuppe


Zwiebel und Pilze und Tofu anbraten, mit ein bisschen Salz und Pfeffer

Zusammen mit roten Linsen (oder irgendwelche Linsen) und Wasser in einen Topf, kochen bis die Linsen durch sind

Spinat passt auch gut rein  
Knoblauch  
Bockshornkleesaat  
Kurkuma  
[Ingwer Grundstock](../zutaten/ingwer_grundstock.md)  
Gemüsebrühe (die Steinpilzbrühe aus dem Reformhaus geht auch immer an alles)

Kartoffeln in Stücke, Öl, Salz, Pfeffer, in den Ofen, dann in die Suppe


Das Internet behauptet, dass Linsen (und Hülsenfrüchten generell) wichtige Aminosäuren, Methionin und Cystin, fehlen. Die gibts aber in allerlei Getreide (vor allem Reis). Es bietet sich also an, zur Linsensuppe Brot zu essen, oder vielleicht noch Reis rein zu tun.

