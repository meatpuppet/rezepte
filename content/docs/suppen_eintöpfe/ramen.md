# "Ramen"

Disclaimer: ich hab keine Ahnung wie man Ramen kocht, das hier ist also "Ramen".

Alles im Rezept ist optional, Resultate schwanken zwischen heißem Wasser und dicker würziger Suppe die man kaum schafft.


Rezept für eine große Schüssel:

- 1/2 Zwiebel
- Hefeflocken
- ~1EL Erdnussbutter
- Sojasauce
- Kurkuma, Pfeffer, ...
- ~1 Zehe Knoblauch (Rezepte haben kein Recht über Knoblauchmengen zu entscheiden!)
- [Ingwergrundstock](../zutaten/ingwer_grundstock.md) (etwa eine Messerspitze?)
- Pak-Choi/Spinat/(oder irgendwas anderes blättriges)
- Pilze (Champignons funktionieren gut, andere gehen bestimmt auch)
- optional noch Gemüse- oder Pilzbrühe
- Algenblätter (Nori, in Streifen geschnitten)
- normaler Tofu
- Nudeln: es gibt spezielle Ramennudeln, aber es gehen auch Udon, oder irgendwelche anderen Weizennudeln die der Asiashop so her gibt. Auf meinen steht gerade "Oriental Style Noodle (Pasta)". Schnellkochend ist gut.


Für die Sauce die Erdnussbutter, Hefeflocken, Ingwergrundstock, Sojasauce, die Gewürze in heißem Wasser auflösen. Knoblauch durch eine Presse reindrücken.
Wenn du gefrorenen Spinat hast, am besten jetzt schon rein - Frisches erst kurz vor Schluss.

Dinge wie Zwiebeln, Pilze, Tofu separat (in Sojasauce, wenn du magst) anbraten.

Wenns nur eine Portion wird, können die Nudeln direkt in die Brühe, ansonsten separat in gut gesalzenem Wasser kochen. Man kann die nachher gut mit Stäbchen direkt aus dem Wasser fischen und auf 2 Schüsseln verteilen. Nachher einfach mit der Brühe auffüllen und was man in der Pfanne hat drauf legen.

Garnieren mit den Algenblättern.

![ramen](images/ramen.png)
