# grünkohl kürbis reis (eintopf, je nachdem..)


    1/2 hokkaido
    1 zwiebel
    grünkohl
    knoblauch
    kokosnussmilch
    frühlingszwiebeln
    reis

reis aufsetzen.

kürbis und zwiebel klein schneiden, zusammen anbraten, dann ein einen topf mit ein bisschen wasser, (nicht zuviel, da kommt noch die kokosmilch dazu) kochen.

knoblauch zerdrücken, dazu tun.

kokosmilch dazu.

irgendwann den reis dazu, ggf funktioniert das auch wirklich als eintopf - sprich, alles zusammen kochen. ich hab den reis aber einzeln gemacht.

an gewürzen geht gut dran:

- gemüsebrühe
- muskat
- [ingwer grundstock](../zutaten/ingwer_grundstock.md)
- (nicht zu viel) paprika (räucherpaprika!)




