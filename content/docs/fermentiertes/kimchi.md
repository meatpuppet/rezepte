# kimchi

von [fairment](https://www.fairment.de/rezepte-artikel/kimchi-rezept/) geklaut


das kimchi selbst:

    1 chinakohl
    1-2 möhren
    1 kleiner weißer rettich
    2 frühlingszwiebeln
    salz
    1el noriflocken



für die kimchipaste:

    3-4 knoblauchzehen
    1 zwiebel
    2-3cm ingwer
    20-40g chiliflocken
    2el misopaste oder sojasauce





- chinakohl in quadrate schneiden
- möhren und rettich raspeln
- frühlingszwiebeln schneiden

dann alles wiegen.
auf 1kg gemüse kommen 20g salz.
salz in das gemüße kneten, bis es im eigenen saft steht.

- alles für die kimchipaste in einen mixer, mixen
- kimchipaste zum gemüse, vermengen
- schicht für schicht ins glas, gemüse sollte mit flüssigkeit bedeckt sein
- oben 3cm platz lassen
- verschließen, beschriften mit datum
- die erste woche bei raumtemperatur lassen
- danach in den kühlschrank
- nach 2-3 wochen fertig
- hält sich einige monate



