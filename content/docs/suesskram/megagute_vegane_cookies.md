# megagute vegane cookies

für 5 große cookies

- 55g rapsöl
- 100g Rohrohrzucker
- 5g Vanillin
- 50g Haferdrink
- 300g Weizenmehl
- 5g Backpulver
- ein Hauch Salz
- 200g Schokolade

Rapsöl, Zucker, Haferdrink mischen

Rest auch dazu, vermengen bis der Teig glatt ist.

Kugeln formen, aufs Blech, bisschen platt drücken - 15 Minuten bei 190 Grad backen.

