# oreotorte

#todo

https://plantbasedcookingshow.com/2020/09/26/double-stuffed-oreo-cake/

Prep Time: 15 Minutes  Cooking Time: 35 Minutes
Rating: 3.9/5 
 ( 26 voted )
INGREDIENTS
Cake
1 cup almond flour
2 heaping cups rolled oats, ground into flour
1 1/2 teaspoon baking powder
1 teaspoon baking soda
3/4 cups black cocoa powder
1 1/2 cups pitted dates
1 teaspoon vanilla extract
3 cups soy milk (or plant milk of your choice)

Frosting
2 cups cashews  (soaked for 2 hours)
1 cup pitted dates
1 teaspoon vanilla extract
1 tablespoon lemon juice
1 cup water

INSTRUCTIONS
Preheat oven to 350°.
Place the almond flour, oat flour, baking powder, baking soda and cocoa powder into a bowl and use a whisk to stir.
Add the dates, vanilla and soy milk to a blender and blend until there are no date chunks left.
Pour mixture into the flour mixture and stir well.
Pour equally into two round parchment lined cake pans.
Bake for 35 minutes.
Take cakes out of oven and let cool.
Place the frosting ingredients into a blender and blend until smooth.
Once cakes are cool, place the first cake onto a cake stand or plate.
Pour half of the frosting onto the cake and spread almost to the edge.
Place the other cake on top and pour the remaining frosting on the cake. 
Spread frosting to the edge of the cake. 
Keeps in the fridge for 4-5 days
