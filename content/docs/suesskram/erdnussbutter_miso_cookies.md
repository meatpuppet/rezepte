# Erdnussbutter-Miso Cookies

auf basis von [diesem rezept](https://www.kitchenstories.com/de/rezepte/erdnussbutter-cookies-mit-miso)


- 200g feiner Rohrzucker
- 1TL Natron
- 1/2TL Backpulver
- 3/4TL Salz
- 250g Erdnussbutter
- irgendein Ersatz für 1 Ei
- 40g weiße Misopaste

Zucker, Natron, Backpulver und Salz mischen, dann zusammen mit der Erdnussbutter aufschlagen.

Ei und Misopaste dazu, weiter aufschlagen.

Backofen auf 180 Grad vorheizen.

In Keksgröße auf ein Backblech verteilen (die zerlaufen noch!!)

Etwa 10 Minuten backen, danach noch 10min abkühlen lassen.

Wenn du alle aufgegessen hast, hast du ein halbes Glas Erdnussbutter und ne halbe Packung Zucker gegessen.

![cookies, vorher](images/peanutmisocookies_before.jpeg)
![cookies, nachher](images/peanutmisocookies_after.jpeg)
