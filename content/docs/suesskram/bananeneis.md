---
title: "Bananeneis"
date: 2022-05-27T19:23:12+02:00
draft: false

---

# Bananeneis

Eine braun gewordene gefrorene Banane[^1] zusammen mit einem Löffel Geschmack[^2] und ein bisschen Flüssigkeit[^3] pürieren. Wird je nach Flüssigkeitsmenge ein Shake.

Wenns ein Shake wird, drüber nachdenken ob Schokotropfen, oder einfach klein geschnittene Bitterschokolade rein sollten!


[^1]: vorm einfrieren schälen und in Stücke brechen
[^2]: Kakao, Erdnuss, Erdbeeren...
[^3]: irgendeine Milch, notfalls Wasser


