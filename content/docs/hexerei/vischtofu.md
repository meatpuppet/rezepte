# visch-tofu

zutaten:
- tofu
- noriblätter (die man auch für sushi nimmt)
- grüner tee
- sojasauce

tofuklötze einfrieren und auftauen, damit die das wasser besser abgeben.

dann längs in 2 dünnere scheiben schneiden, und zwischen zwei brettern ausdrücken.

grünen tee kochen (nicht das kochende wasser über den tee! sonst wird er bitter! eher so 80 grad.)

die ausgedrückten tofuscheiben in noriblätter wickeln (die vom sushi machen), und alle in eine form packen, in die sie möglichst genau rein passen.

mit dem tee übergießen, bis sie fast ganz bedeckt sind. den rest mit sojasauce auffüllen. (aber nicht zu viel!)

abkühlen lassen und über nacht in den kühlschrank.

danach kann man die dinger zum beispiel anbraten oder backen! :)


![Vischbrötchen](images/visch.jpg)


