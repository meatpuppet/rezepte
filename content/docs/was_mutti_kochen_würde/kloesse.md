---
title: "Kartoffelklöße"
---

# Kartoffelklöße

800g kartoffeln
~100g stärke
150g wasser
ein bisschen salz

kartoffeln kochen, abkühlen lassen  
kartoffeln mit dem rest vermatschen, in gewünschte form bringen (kleine "patties" gehen auch!)

vorsichtig in heißes wasser, 20 minuten fast kochen (nicht zu sehr, damit alles in form bleibt)

kann man nachher nochmal super anbraten falls man möchte. ansonsten einfach braune sauce und rotkraut dazu



