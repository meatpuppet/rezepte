## Hafermilch

(ausprobieren: mit gekeimtem Hafer)


- 1l kaltes[^1] Wasser mit ~50g Haferflocken pürieren[^2] 
- absieben (einfach durch ein feines Sieb reicht eigentlich, je nachdem was du damit vor hast.)

Eine Prise Salz dazu.


[^1]: Mit warmem Wasser wirds schleimig, und entsprechend schlecht zu sieben.
[^2]: Je nach Geschmack auch mehr. Am besten erst mit weniger Wasser, dann nachgießen

