# Ingwer Grundstock

![ingwer grundstock in einem glas](images/ingwergrundstock.jpg)

Abgeguckt von einem Thermomix rezept.

Ingwer schälen, und etwa 1/5 des Ingwergewichts in Salz dazu tun. (also auf 100g Ingwer etwa 20g Salz)

Pürieren.


Ideal, um [Wintersuppen](../suppen_eintöpfe/linsensuppe.md) zu würzen.


Wenn ihr schält wie ich, ergeben 170g Ingwer (eine große Knolle) etwa 130g wenn geschält, die wiederum ~26g Salz brauchen.
In so ein kleines Glas gehen dann etwa 2 solcher Knollen.

Sollte eigentlich ewig halten. Mindestens diesen Winter. :)


