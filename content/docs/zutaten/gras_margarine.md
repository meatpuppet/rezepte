# weed margarine/butter/...

Auf 250g Margarine könnte man, wenn man wollte, so etwa 5g Gras tun.

Gras [decarboxylieren](https://de.wikipedia.org/wiki/Decarboxylierung): Gras in einen ofenfesten Behälter, einen lockeren Deckel drauf, und bei 100°c für eine halbe Stunde in den Ofen

Danach das decarboxylierte Gras in etwas Wasser, quellen lassen.

Dann das aufgeweichte Gras in geschmolzene Margarine: die sollte nicht heißer als 80°c sein, und muss über 2 Stunden die Temperatur halten. am besten in einem Wasserbad machen!

Danach sollte die Margarine über Nacht abkühlen.


