# Teriyaki Mie Nudeln

## tempeh

    200g Tempeh
    3 Knoblauchzehen
    2el Sojasauce
    1el Balsamicoessig
    1el Agavendicksaft

## wok

    200g Mie Nudeln
    1/2 Brokkoli
    2 Karotten
    1 kl. Zucchini
    2 Frühlingszwiebeln
    2 Paprika
    100g Zuckerschoten
    2 Knoblauch
    Ingwer
    100g Bambussprossen
    100g Mungbohnensprossen
    2el Erdnussöl

## teriyakisauce

    50ml Sojasauce
    2el Agavendicksaft
    2el Reisessig
    2el Sesamsamen
    1el Stärke


Tempeh klein schneiden, mit dem Rest verrühren, 1h ziehen lassen.

Mie Nudeln kochen, abschütten.

Gemüse schneiden.

Möhren und Tempeh (ohne Marinade) im Wok braten.

Danach Brokkoli, Paprika, Zucchini dazu, braten.

Temperatur runter drehen - dann Frühlingszwiebeln und Zuckerschoten, Sprossen dazu.

Für die Teryakisauce: Sojasauce, Agavendicksaft, Essig, Sesam mit 100ml Wasser mischen, aufkochen.

Separat die Stärke mit kaltem Wasser anrühren, in die Sauce geben, erneut aufkochen bis sie andickt.

Zum Schluss die Sauce und Nudeln übers Wok Gemüse, gut unterheben.

Alles Gute! :)
