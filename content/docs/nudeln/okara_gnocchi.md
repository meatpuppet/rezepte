# okara gnocchi

abgeschaut von [hier](https://cherry-secret-recipe.com/mix/1460-recipe-of-homemade-okara-gnocchi/)


    ~2 große Tassen Okara
    1 TL Salz (gestrichen - das war so schon sehr salzig)
    ~12TL Kartoffelmehl (nicht ganz Flach, aber auch keine Haufen)
    ~2TL Mehl (Haufen!)
    ~10TL Wasser

    (...sorry, irgendwann lege ich mir mal eine Waage zu.)


Okara, Salz, Kartoffelmehl mischen, Wasser dazu, verkneten.

Zu Gnocchi formen, 3 Minuten kochen (bis sie oben schwimmen).

Nachher braten oder so.

![Okara Gnocchi](images/okara_gnocchi.jpg "eine einzelne gebratene Okara Gnocchi (mir ist ein bisschen spät eingefallen dass ein Foto cool wär)")

