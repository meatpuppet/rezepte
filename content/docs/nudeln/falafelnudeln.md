# Falafelnudeln / Falafelini

Nudeln kochen, sofort nach dem Wasser abschütten Falafelpulver auf die Nudeln, gut umrühren und nochmal den den Deckel auf den Topf machen.
Zwei, drei Minuten warten.

Dann die Nudeln in eine gut fettige, heiße Pfanne bis die nudeln von aussen falafelig aussehen.

Auch gut: Falafelini. (Ja, Falafel-Tortellini.)

![Falafelini](images/falafelini.jpg "eine Pfanne voll gebratener Falafelini")

