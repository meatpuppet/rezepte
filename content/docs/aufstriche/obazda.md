# Obazda

geklaut von [utopia](https://utopia.de/ratgeber/veganer-obazda-rezept-fuer-den-bayerischen-dip/)

Achtung: Cashews müssen über Nacht einweichen!

```
150g Cashewkerne
1el Apfelessig
2el Hefeflocken
1 Knoblauch
50ml Wasser
100g Margarine
400g Räuchertofu
1 kleine Zwiebel, rot
1/2tl Paprikapulver süß
1/4tl Paprika scharf
1tl Kümmel
30ml Weißbier
```

Cashewkerne einweichen, am besten über Nacht.  

Cashews pürieren, zusammen mit Essig, Hefeflocken, Knoblauch, 50ml Wasser - die Masse sollte cremig sein

Margarine aufschlagen, Tofu reinkrümeln, Zwiebel dazuhacken, Paprikapulver, Kümmel, Weißbier dazu.

Cashewkernmasse unterheben.

Eine Stunde ziehen lassen!

---

Schnelle Supermarktalternative:

![obazda einkauf: margarine, zwei weich"käse", ein bier, vegane streichcreme, ein bund frühlingszwiebeln auf einem holztisch](images/obazda_1.jpeg)
![der fertige obazda. eine blechdose mit der leicht orangen pampe, mit ein paar zwiebelscheiben obendrauf. auf einem holzisch, im hintergrund wiese.](images/obazda_2.jpeg)

```
- (veganer) Weichkäse normal und würzig
- (veganer) Frischkäse
- ca. 70g Margarine
- vegane Creme
- kleine rote Zwiebel
- Frühlingszwiebel oder Schnittlauch
- Paprikapulver geräuchert und scharf
- Rauchsalz gemahlen
- Peffer
- gem. kümmel
- ein Schluck Bier
```
