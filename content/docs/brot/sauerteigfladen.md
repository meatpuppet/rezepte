## Sauerteigfladen

Ich war zu faul zum backen, also hab ich meinen Sauerteigansatz einfach über in Sojasauce gebratenen Tofu und Pilze gekippt.

Faulheit siegt!

![sauerteigfladen_1](images/sauerteigfladen_1.jpeg "ein halb gebackener sauerteigfladen in einer pfanne. man sieht noch tofu und pilze.")

![sauerteigfladen_2](images/sauerteigfladen_2.jpeg "dasselbe, aber schon ein bisschen gebratener")

![sauerteigfladen_3](images/sauerteigfladen_3.jpeg "und der fladen von der anderen seite")



