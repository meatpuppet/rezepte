---
title: "Roggensauerteigbrot"
---
# Roggensauerteigbrot

Basierend auf [ohnemist.de/bauernbrot-mit-sauerteig-roggenvollkornmehl](https://www.ohnemist.de/bauernbrot-mit-sauerteig-roggenvollkornmehl)

![brot, frisch aus dem topf :)](images/bread.jpg)

![brotscheiben](images/bread_slices.jpg)

- 250ml warmes Wasser
- 50g [Sauerteig](./sauerteig.md)
- 220g Roggenvollkornmehl

Verrühren, 15h reifen lassen.

- 18g Salz
- 350ml warmes Wasser
- 440g Roggenmehl

Auf den gereiften Teig schütten, alles verrühren, 3h reifen lassen.

- 340g Weizenmehl
- evtl. Wasser, wenn zu fest

Gut verkneten, 1-6h gehen lassen. Volumen sollte sich 1/3 erhöhen.


Ofen auf 250°C vorheizen.

(Evtl. Schüssel mit Wasser in den Ofen stellen!)  
Erst bei hoher Temperatur backen, nach 10-20min auf 180-220°C runter drehen.
Insgesamt ca. 75 Minuten backen.

Funktioniert auch sehr gut im gusseisernen Topf!


### variation

in 2. Schritt 
    
    20g Salz,
    350ml Wasser
    620g Weizenmehl

(3. Schritt weg lassen)

wenn man den ersten schritt abends um 18:00 ansetzt, ist der am nächsten morgen um 10 uhr durch, und man kann theoretisch um 16 uhr spätestens backen.
tendenziell eher früher.

