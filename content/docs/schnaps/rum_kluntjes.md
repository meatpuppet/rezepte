# Rum Kluntjes/Kandis

```
Rum (am besten 54%)
Zucker
(brauner) Kandiszucker

Zimtstangen (optional)
Ingwer (optional)
Orange (optional)
...
```

Rum mit Zucker sättigen.  
Ein Liter Rum löst richtig viel Zucker - etwa 1:1 würde ich sagen, aber ich hab den Überblick verloren.  
Am besten immer nachkippen, bis ein bisschen Zucker übrig bleibt, der sich nichtmehr auflöst. Wir machen das, damit der Rum später nicht den Kandis auflöst.

Dann den gesättigten Rum über Zimtstangen, geschälten, geschnittenen Ingwer, Orangenscheiben schütten.  
Zwei Wochen warten, immer mal schütteln.

Danach durch ein Sieb geben - durch die Gewürze wird der Rum sonst irgenwann Bitter. (Eventuell kann man den Ingwer drin lassen? Die Ingwer schärfe braucht sehr lange, um sich zu lösen, und bitter wird der auch nicht.)
Also die Zimtstangen, Orangen usw. raus nehmen, und mit dem Rum ein Glas voll Kandis auffüllen.

